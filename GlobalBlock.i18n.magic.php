<?php
/**
 * Curse Inc.
 * Global Block
 * Manage global blocks for anonymous and registered accounts.
 *
 * @author		Alexia E. Smith
 * @copyright	(c) 2017 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		GlobalBlock
 * @link		https://gitlab.com/hydrawiki
 *
**/

$magicWords = [];
$magicWords['en'] = [
	'globalblock' => array(0, 'globalblock'),
	'globalblocklist' => array(0, 'globalblocklist'),
];
